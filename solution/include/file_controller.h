#ifndef FILE_CONTROLLER_H
#define FILE_CONTROLLER_H

#include <stdio.h>

enum file_status {
    OPEN_OK = 0,
    OPEN_FAILED,
    CLOSE_OK,
    CLOSE_FAILED
};

enum file_status open_file(const char* filename, FILE** file, const char* type);
enum file_status close_file(FILE* file);

#endif
