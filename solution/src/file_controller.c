#include <stdio.h>

#include "file_controller.h"

enum file_status open_file(const char* filename, FILE** file, const char* type) {
    *file = fopen(filename, type);
    return *file == NULL ? OPEN_FAILED : OPEN_OK;
}

enum file_status close_file(FILE* file) {
    return (fclose(file) == 0) ? CLOSE_OK : CLOSE_FAILED;
}
