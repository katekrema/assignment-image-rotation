#include <stdio.h>

#include "bmp_image_controller.h"

static const uint16_t BMP_SIGNATURE = 0x4D42;
static const uint32_t BMP_HEADER_SIZE = 40;
static const uint16_t PLANES = 1;
static const uint16_t BIT_COUNT = 24;
static const uint32_t COMPRESSION_TYPE = 0;
static const uint32_t NONE = 0;

// Функция для вычисления padding
// Функция для вычисления padding
static uint32_t calculate_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

// Функция для создания заголовка BMP
static struct bmp_header create_bmp_header(const struct image* image) {
    uint32_t image_size = (image->height) * (sizeof(struct pixel) * (image->width) + calculate_padding(image->width));

    struct bmp_header header = {
            .bfType = BMP_SIGNATURE,
            .bfileSize = sizeof(struct bmp_header) + image_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_HEADER_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION_TYPE,
            .biSizeImage = image_size,
            .biXPelsPerMeter = NONE,
            .biYPelsPerMeter = NONE,
            .biClrUsed = NONE,
            .biClrImportant = NONE
    };

    return header;
}

// Функция для чтения заголовка BMP из файла
static enum read_status read_bmp_header(FILE* in, struct bmp_header* header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        // fread вернул не 1, что может означать ошибку или конец файла
        return feof(in) ? READ_EOF : READ_INVALID_HEADER;
    }

    if (header->bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* image) {
    struct bmp_header header;
    enum read_status status = read_bmp_header(in, &header);
    if (status != READ_OK) {
        return status;
    }

    *image = create_image(header.biWidth, header.biHeight);
    if (!image->data) {
        return READ_INVALID_BITS;
    }

    uint32_t padding = calculate_padding(header.biWidth);
    for (uint32_t y = 0; y < header.biHeight; y++) {
        if (fread(&image->data[y * header.biWidth], sizeof(struct pixel) * header.biWidth, 1, in) != 1) {
            // fread вернул не 1, что может означать ошибку или конец файла
            destroy_image(*image);
            return feof(in) ? READ_EOF : READ_INVALID_BITS;
        }
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* image) {
    struct bmp_header header = create_bmp_header(image);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) == 0) {
        return WRITE_ERROR;
    }

    uint32_t padding = calculate_padding(image->width);
    uint32_t byte = 0;
    for (uint32_t y = 0; y < image->height; y++) {
        if (fwrite(&image->data[y * image->width], sizeof(struct pixel) * image->width, 1, out) == 0) {
            return WRITE_ERROR;
        }
        if (padding != 0 && fwrite(&byte, padding, 1, out) == 0) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

