#include "image.h"
#include "rotator.h"

struct image rotate(struct image const source) {
    struct image rotated = create_image(source.height,source.width);
    if (!rotated.data) return rotated;

    for (uint64_t x = 0; x < source.width; x++) {
        for (uint64_t y = 0; y < source.height; y++) {
            rotated.data[x * rotated.width + y] = source.data[(source.height - 1 - y) * source.width + x];
        }
    }

    return rotated;
}
