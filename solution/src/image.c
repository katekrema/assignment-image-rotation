#include <stdio.h>
#include <stdlib.h>

#include "image.h"

struct image create_image(const uint64_t width, const uint64_t height) {
    // Проверка на действительность ширины и высоты
    if (width <= 0 || height <= 0) {
        fprintf(stderr, "Недопустимые значения ширины или высоты изображения.\n");
        exit(EXIT_FAILURE);
    }

    struct image new_image;
    new_image.width = width;
    new_image.height = height;

    new_image.data = (struct pixel*)malloc(sizeof(struct pixel) * width * height);

    // Проверка на успешное выделение памяти
    if (new_image.data == NULL) {
        fprintf(stderr, "Ошибка выделения памяти для изображения.\n");
        exit(EXIT_FAILURE);
    }

    return new_image;
}

void destroy_image(struct image image) {
    free(image.data);
    image.height = 0;
    image.width = 0;
}
