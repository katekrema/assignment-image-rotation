#include <stdio.h>

#include "bmp_image_controller.h"
#include "file_controller.h"
#include "rotator.h"

int main(int argc, char** argv) {
    FILE* file;

    if (argc != 3) {
        fprintf(stderr, "Используется: %s <in> <out>\n", argv[0]);
        return EXIT_FAILURE;
    }

    if (open_file(argv[1], &file, "rb") != OPEN_OK) {
        perror("Не удалось открыть изображение\n");
        return EXIT_FAILURE;
    }

    struct image image;
    int image_status = from_bmp(file,&image);
    if (image_status != READ_OK) {
        fprintf(stderr, "Не удалось прочитать изображение: %d\n", image_status);
        return EXIT_FAILURE;
    }

    if (close_file(file) != CLOSE_OK) {
        destroy_image(image);
        return EXIT_FAILURE;
    }

    struct image rotated = rotate(image);
    if (!rotated.data) {
        fprintf(stderr, "Не удалось создать новое изображение с помощью rotate()\n");
        destroy_image(image);
        destroy_image(rotated);
        return EXIT_FAILURE;
    }

    destroy_image(image);

    if (open_file(argv[2], &file, "wb") != OPEN_OK) {
        perror("Не удалось открыть изображение\n");
        destroy_image(rotated);
        return EXIT_FAILURE;
    }

    int rotated_status = to_bmp(file,&rotated);
    if (rotated_status != WRITE_OK) {
        fprintf(stderr, "Не удалось записать новое изображение: %d\n", rotated_status);
        destroy_image(rotated);
        return EXIT_FAILURE;
    }

    if (close_file(file) != CLOSE_OK) {
        return EXIT_FAILURE;
    }

    destroy_image(rotated);

    printf("%s\n","Успешно выполнено!");
    return 0;
}
